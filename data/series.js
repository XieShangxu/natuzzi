var series = {
    ni: {
        shafa: [{
                id: '2957',
                name: 'Philo',
                image: '2957 Philo.jpg'
            }, {
                id: '2964',
                name: 'Svevo',
                image: '2964 Svevo.jpg'
            }, {
                id: '2981',
                name: 'Herman',
                image: '2981 Herman.jpeg'
            }, {
                id: '2987',
                name: 'Jeremy',
                image: '2987 Jeremy.jpg'
            }, {
                id: '2994',
                name: 'Ido',
                image: '2994 Ido.jpg'
            }, {
                id: '3000',
                name: 'La Scala',
                image: '3000 La Scala.jpg'
            }, {
                id: '2907',
                name: 'Fidelio',
                image: '2907 Fidelio.jpg'
            }, {
                id: '2904',
                name: 'Dorian',
                image: '2904 Dorian.jpg'
            }, {
                id: '2954',
                name: 'Iago',
                image: '2954 Iago.jpg'
            }, {
                id: '2996',
                name: 'Ilia',
                image: '2996 Llia.jpg'
            }, {
              id: '2570',
              name: 'Avana',
              image: '2570 AVANA.jpg'
            }, {
              id: '2571',
              name: 'Surround',
              image: '2571 SURROUND.jpg'
            }, {
              id: '2788',
              name: 'Armonia',
              image: '2788 ARMONIA.jpg'
            }, {
              id: '2789',
              name: 'Note',
              image: '2789 NOTE.jpg'
            }, {
              id: '2826',
              name: 'Borghese',
              image: '2826 BORGHESE.jpg'
            }, {
              id: '2834',
              name: 'Tempo',
              image: '2834 TEMPO.jpg'
            }, {
              id: '2847',
              name: 'Viaggio',
              image: '2847 VIAGGIO.jpg'
            }, {
              id: '2906',
              name: 'Don Giovanni',
              image: '2906 DONGIOVANNI.jpg'
            }, {
              id: '2911',
              name: 'Long Beach',
              image: '2911 LONGBEACH.jpg'
            }, {
              id: '2913',
              name: 'Aura',
              image: '2913 AURA.jpg'
            }, {
              id: '2955',
              name: 'Aplomb',
              image: '2955 APLOMB.jpg'
            }, {
              id: '2966',
              name: 'Ergo',
              image: '2966 ERGO.jpg'
            }, {
              id: '2989',
              name: 'Agora',
              image: '2989 AGORA.jpg'
            }, {
              id: '3029',
              name: 'Galaxy',
              image: '3029 GALAXY.jpg'
            }, {
              id: 'AFTEREIGHT',
              name: 'Aftereight',
              image: 'AFTEREIGHT.jpg'
            }, {
              id: 'KENDO',
              name: 'Kendo',
              image: 'KENDO.jpg'
            }, {
              id: 'LEM',
              name: 'Lem',
              image: 'LEM.jpg'
            }, {
              id: 'LUNA',
              name: 'Luna',
              image: 'LUNA.jpg'
            }, {
              id: 'MELPOT',
              name: 'Melpot',
              image: 'MELPOT.jpg'
            }],
        chuang: [{
                id: 'L017',
                name: 'Fenice',
                image: 'L017 Fenice.jpg'
            }, {
                id: 'L018',
                name: 'Svevo',
                image: 'L018 Svevo.jpg'
            }, {
                id: 'L011',
                name: 'Piuma',
                image: 'L011 Piuma.jpg'
            }, {
                id: 'L016',
                name: 'Dolcevita',
                image: 'L016 Dolcevita.jpg'
            }, {
                id: 'L014',
                name: 'Diamante',
                image: 'L014 Diamante.jpg'
            }, {
                id: 'L012',
                name: 'Onda',
                image: 'L012 ONDA.jpg'
            }, {
                id: 'L013',
                name: 'Vela',
                image: 'L013 VELA.jpg'
            }, {
                id: 'L015',
                name: 'Oasi',
                image: 'L015 OASI.jpg'
            }, {
              id: 'Amber',
              name: 'Amber',
              image: 'AMBER.jpg'
            }],
        canzhuo: [{
                id: 'E014',
                name: 'Accademia',
                image: 'E014 ACCADEMIA.jpg'
            }, {
                id: 'E016',
                name: 'Leggero',
                image: 'E016 LEGGERO.jpg'
            }, {
                id: 'E012',
                name: 'Ramo',
                image: 'E012 Ramo.jpg'
            }, {
                id: 'E015',
                name: 'Hex',
                image: 'E015 HEX.jpg'
            }, {
                id: 'E020',
                name: 'Amber',
                image: 'E020 AMBER.jpg'
            }, {
                id: 'E021',
                name: 'Kendo',
                image: 'E021 KENDO.jpg'
            }, {
                id: 'E024',
                name: 'Phantom',
                image: 'E024 PHANTOM.jpg'
            }],
        canyi: [ {
                id: 'CH01',
                name: 'Ambra',
                image: 'CH01 ambra.jpg'
            }, {
                id: 'CH00',
                name: 'Lisa',
                image: 'CH00 Lisa.jpg'
            }, {
                id: 'C014',
                name: 'Flio',
                image: 'C014 FLIO.jpg'
            }, {
                id: 'C015',
                name: 'Clio',
                image: 'C015 CLIO.jpg'
            }, {
                id: 'CH09',
                name: 'Sonata',
                image: 'CH09 SONATA.jpg'
            }, {
                id: 'CH10',
                name: 'Mama',
                image: 'CH10 MAMA.jpg'
            }],
        chaji: [{
                id: 'T140',
                name: 'Labirinto',
                image: '1.T140.jpg'
            }, {
                id: 'T148',
                name: 'Svevo',
                image: '2.T148.jpg'
            }, {
                id: 'T156',
                name: 'Herman',
                image: 'T156 HERMAN.jpg'
            }, {
                id: 'T157',
                name: 'Ido',
                image: '4.T157.jpg'
            }, {
                id: 'T150',
                name: '',
                image: '5.T150.jpg'
            }, {
                id: 'T163',
                name: 'Kendo',
                image: 'T163 KENDO.jpg'
            }],
        guilei: [{
                id: 'W010',
                name: 'Kubika',
                image: 'W010 KUBIKA.jpg'
            }, {
                id: 'W013',
                name: 'Plisse',
                image: 'W013 Plisse.jpg'
            }, {
                id: 'W014',
                name: 'Accademia',
                image: 'W014 ACCADEMIA.jpg'
            }, {
                id: 'W018',
                name: 'Amber',
                image: 'W018 AMBER.jpg'
            }, {
                id: 'W019',
                name: 'Leggero',
                image: 'W019 LEGGERO.jpg'
            }, {
                id: 'B002',
                name: 'Mondrian',
                image: 'B002.jpg'
            }, {
                id: 'B004',
                name: 'Munari',
                image: 'B004.jpg'
            }, {
                id: 'B007',
                name: 'Svevo',
                image: 'B007.jpg'
            }, {
                id: 'B005',
                name: 'Euclide',
                image: 'B005.jpg'
            }],
        woshi: [{
                id: 'W010',
                name: 'Kubika',
                image: 'W010 KUBIKA.jpg'
            }, {
                id: 'W013',
                name: 'Plisse',
                image: 'W013 Plisse.jpg'
            }, {
                id: 'B002',
                name: 'Mondrian',
                image: 'B002.jpg'
            }, {
                id: 'B004',
                name: 'Munari',
                image: 'B004.jpg'
            }, {
                id: 'B007',
                name: 'Svevo',
                image: 'B007.jpg'
            }, {
                id: 'B005',
                name: 'Euclide',
                image: 'B005.jpg'
            }, {
                id: 'A846',
                name: 'Vogue',
                image: 'A846.jpg'
            }, {
                id: 'A875',
                name: 'Royal',
                image: 'A875.jpg'
            }, {
                id: 'A876',
                name: 'Pausa',
                image: 'A876.jpg'
            }],
        canting: [{
                id: 'E014',
                name: 'Accademia',
                image: 'E014 ACCADEMIA.jpg'
            }, {
                id: 'E016',
                name: 'Leggero',
                image: 'E016 LEGGERO.jpg'
            }, {
                id: 'E012',
                name: 'Ramo',
                image: 'E012 Ramo.jpg'
            }, {
                id: 'CH01',
                name: 'Ambra',
                image: 'CH01 ambra.jpg'
            }, {
                id: 'CH00',
                name: 'Lisa',
                image: 'CH00 Lisa.jpg'
            }, {
                id: 'A889',
                name: 'Cibo',
                image: 'A889 cibo.jpg'
            }, {
                id: 'A891',
                name: 'Orme',
                image: 'A891 orme.jpg'
            }, {
                id: 'A892',
                name: 'Orlo',
                image: 'A892 orlo.jpg'
            }],
        shipin: [{
                id: 'R849',
                name: 'Affresco',
                image: 'R849 AFFRESCO.jpg'
            }, {
                id: 'R859',
                name: 'Seti',
                image: 'R859 SETI.jpg'
            }, {
                id: 'R860',
                name: 'Macrus',
                image: 'R860 MACRUS.jpg'
            }, {
                id: 'A872',
                name: 'Please',
                image: 'A872.jpg'
            }, {
                id: 'A873',
                name: 'Spiga',
                image: 'A873.jpg'
            }, {
                id: 'A879',
                name: 'Lady',
            }, {
                id: 'A880',
                name: 'Age',
            }, {
                id: 'A827',
                name: 'Istante',
                image: 'A827.jpg'
            }, {
                id: 'A828',
                name: 'Zerotondo',
                image: 'A828.jpg'
            }, {
                id: 'Y006',
                name: 'Numa',
                image: 'Y006.jpg'
            }, {
                id: 'A837',
                name: 'Basket Mapa',
                image: 'A837.jpg'
            }, {
                id: 'A844',
                name: 'Baby Alpaca',
            }, {
                id: 'L490',
                name: 'Cosmo',
                image: 'L490 COSMO.jpg'
            }, {
                id: 'L491',
                name: 'Martini',
                image: 'L491 MARTINI.jpg'
            }, {
                id: 'A829',
                name: 'Profumo Onice',
                image: 'A829 ONICE.jpg'
            }, {
                id: 'A830',
                name: 'Perla',
                image: 'A830 PERLA.jpg'
            }, {
                id: 'A831',
                name: 'Rubino',
                image: 'A831 RUBINO.jpg'
            }, {
                id: 'A832',
                name: 'Ambra',
                image: 'A832 AMBRA.jpg'
            }],
        keting: [{
                id: 'T140',
                name: '',
                image: '1.T140.jpg'
            }, {
                id: 'T148',
                name: '',
                image: '2.T148.jpg'
            }, {
                id: 'T156',
                name: '',
            }, {
                id: 'T157',
                name: '',
                image: '4.T157.jpg'
            }, {
                id: 'T150',
                name: '',
                image: '5.T150.jpg'
            }, {
                id: 'R859',
                name: '',
            }, {
                id: 'R860',
                name: '',
            }, {
                id: 'R849',
                name: '',
                image: 'R849.jpg'
            }],
    },
    ne: {
        sanren: [{
                id: 'B969',
                image: 'B969.jpg'
            }, {
                id: 'B895',
                image: 'B895.jpg'
            }, {
                id: 'B970',
                image: 'B970.jpg'
            }, {
                id: 'B993',
                image: 'B993.jpg'
            }, {
                id: 'B888',
                image: 'B888.jpg'
            }, {
                id: 'B868',
                image: 'B868.jpg'
            }, {
                id: 'B872',
                image: 'B872.jpg'
            }, {
                id: 'B977',
                image: 'B977.jpg'
            }, {
                id: 'B875',
                image: 'B875.jpg'
            }, {
                id: 'B939',
            }, {
                id: 'B908',
                image: 'B908.jpg'
            }, {
                id: 'B968',
                image: 'B968.jpg'
            }, {
                id: 'B986',
                image: 'B986.jpg'
            }, {
                id: 'C006',
            }, {
                id: 'B858',
                image: 'B858.jpg'
            }, {
                id: 'B988',
                image: 'B988.jpg'
            }, {
                id: 'C005',
                image: 'C005.jpg'
            }, {
                id: 'B979',
                image: 'B979.jpg'
            }],
        zhuanjiao: [{
                id: 'B632',
                image: 'B632.jpg'
            }, {
                id: 'B814',
                image: 'B814.jpg'
            }, {
                id: 'B969',
                image: 'B969.jpg'
            }, {
                id: 'B993',
                image: 'B993.jpg'
            }, {
                id: 'B796',
                image: 'B796.jpg'
            }, {
                id: 'B940',
                image: 'B940.jpg'
            }, {
                id: 'B983',
                image: 'B983.jpg'
            }],
        gongneng: [{
                id: 'B757',
                image: 'B757.jpg'
            }, {
                id: 'B875',
            }, {
                id: 'B939',
                image: 'B939.jpg'
            }, {
                id: 'B969',
                image: 'B969.jpg'
            }, {
                id: 'B986',
                image: 'B986.jpg'
            }, {
                id: 'B795',
                image: 'B795.jpg'
            }, {
                id: 'B979',
            }, {
                id: 'B842',
                image: 'B842.jpg'
            }, {
                id: 'B883',
                image: 'B883.jpg'
            }, {
                id: 'B995',
                image: 'B995.jpg'
            }],
        danyi: [{
                id: 'B889',
                image: 'B889.jpg'
            }, {
                id: 'B944',
                image: 'B944.jpg'
            }, {
                id: 'B958',
                image: 'B958.jpg'
            }, {
                id: 'C011',
                image: 'C011.jpg'
            }, {
                id: 'B930',
                image: 'B930.jpg'
            }, {
                id: 'B903',
                image: 'B903.jpg'
            }, {
                id: 'B932',
                image: 'B932.jpg'
            }, {
                id: 'B843',
                image: 'B843.jpg'
            }],
        peitao: [{
                id: 'A800',
            }, {
                id: 'R838',
            }, {
                id: 'R851',
            }, {
                id: 'R855',
                image: 'R855.jpg'
            }, {
                id: 'R856',
                image: 'R856.jpg'
            }, {
                id: 'R857',
                image: 'R857.jpg'
            }, {
                id: 'R863',
                image: 'R863.jpg'
            }, {
                id: 'A128',
            }, {
                id: 'A130',
            }, {
                id: 'A131',
            }, {
                id: 'T125',
                image: 'T125.jpg'
            }, {
                id: 'T126',
                image: 'T126.jpg'
            }, {
                id: 'T144',
                image: 'T144.jpg'
            }, {
                id: 'T145',
                image: 'T145.jpg'
            }, {
                id: 'T152',
                image: 'T152.jpg'
            }, {
                id: 'T153',
                image: 'T153.jpg'
            }, {
                id: 'W005',
                image: 'W005.jpg'
            }]
    }
}