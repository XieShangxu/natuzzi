var shop = {
    '保定': {
        ni: [],
        ne: [{
            name: '居然之家',
            store: '',
            address: '保定市七一路与东三环交叉路口居然之家家具家饰馆一层意迪森',
            tel: '13933581391',
        }]
    },
    '北京': {
        ni: [{
            name: '金源居然之家',
            store: 'Beijing Jinyuan Easyhome',
            address: '北京市海淀区远大路1号金源时代购物中心居然之家一层1032',
            tel: '010-88873532',
        }, {
            name: '北居然',
            store: 'Beijing North Easyhome',
            address: '北京市朝阳区北四环东路65号居然之家家之尊5号国际馆三层3-06',
            tel: '010-84629030',
        }, {
            name: '丽泽居然之家',
            store: 'Beijing Lize Easyhome',
            address: '北京市丰台区西三环南路甲27号居然之家丽泽店一层1-111',
            tel: '010-63708202',
        }, {
            name: '东红星',
            store: 'Beijing East Redstar',
            address: '北京市朝阳区东四环中路193号红星美凯龙东四环店一层A8128',
            tel: '010-87951511',
        }, {
            name: '东居然',
            store: 'Beijing East Easyhome',
            address: '北京市朝阳区东三环十里河桥居然之家十里河店8号馆8-1-003',
            tel: '010-67322570',
        }],
        ne: [{
            name: '东红星店',
            store: 'Beijing East Shibo Redstar',
            address: '北京市朝阳区东四环大郊亭桥南甲193号红星美凯龙家居馆二层C8223 意迪森沙发',
            tel: '010-87951689',
        }, {
            name: '东居然店',
            store: 'Beijing East Dayangfanglu Easyhome',
            address: '北京市朝阳区东三环十里河居然之家家具馆 8-5-007 意迪森',
            tel: '010-87366612',
        }, {
            name: '西居然店',
            store: 'Beijing West Yuandalu Easyhome',
            address: '北京市海淀区远大路1号金源居然之家 家居馆B1 006意迪森沙发',
            tel: '010-8886 4380',
        }, {
            name: '北红星店',
            store: 'Beijing North Deshengmen Redstar',
            address: '北京市北四环35号1号院红星美凯龙 A2005 意迪森',
            tel: '010-6486 6425',
        }, {
            name: '北居然店',
            store: 'Beijing North Sihuandonglu Easyhome',
            address: '北京市北四环东路65号居然之家 1-1-004 意迪森沙发',
            tel: '010-84623309',
        }, {
            name: '丽泽居然之家店',
            store: 'Beijing West Lize Easyhome',
            address: '北京市丰台区西三环南路甲27号居然之家2-076意迪森',
            tel: '010-63708135',
        }, {
            name: '城外诚店',
            store: 'Beijing Chengshousilu Chengwaicheng',
            address: '北京市朝阳区成寿寺路308号城外诚家居广场主楼二层B-016',
            tel: '010-87634219',
        }, {
            name: '北京玉泉营居然',
            store: 'Beijing South Yuquanying Easyhome',
            address: '北京市丰台区南三环玉泉营西路58号 居然之家 2-022 意迪森沙发',
            tel: '010-83679050',
        }, {
            name: '北京南集美',
            store: 'Beijing Nanjimei',
            address: '北京市丰台区永外大红门西马场甲14号集美家居5号厅2层52008-4',
            tel: '010-87861322',
        }]
    },
    '蚌埠': {
      ni: [],
      ne: [{
          name: '居然之家蚌埠店',
          store: '',
          address: '安徽省蚌埠市迎宾大道居然之家一楼意迪森',
          tel: '0552-3351166',
      }]
    },
    '长沙': {
      ni: [{
        name: '长沙喜盈门店',
        store: 'Changsha Cimen',
        address: '湖南省长沙市雨花区万家丽路喜盈门家居国际广场六楼中厅6023',
        tel: '0731-85646770'
      }],
      ne: []
    },
    '常州': {
        ni: [],
        ne: [{
            name: '月星家居',
            store: 'Changzhou Tongjianglu Moonstar',
            address: '常州市新北区通江中路588号月星家居1楼A022',
            tel: '0519-83861233',
        }, {
          name: '飞龙路红星',
          store: 'Changzhou Feilonglu Redstar',
          address: '常州市飞龙东路72号红星家世界2楼C8019',
          tel: '0519-81883783',
        }]
    },
    '成都': {
        ni: [{
            name: '富森美',
            store: 'Chengdu South Fusenmei',
            address: '成都高新区盛和二路18号富森美家居（南门店）一层',
            tel: '028-61559167',
        }, {
            name: '北富森美',
            store: 'Chengdu North Fusenmei',
            address: '成都市成华区三环路川陕立交桥外侧蓉都大道将军路68号富森美家居进口馆',
            tel: '028-80580330',
        }],
        ne: [{
            name: '富森美店',
            store: 'Chengdu Shengheerlu Fusenmei',
            address: '成都市盛和二路18号，富森.美家居家具楼B1楼0027-0029号',
            tel: '028-82830080',
        }, {
            name: '富森美北门店',
            store: 'Chengdu Beimen Fusenmei',
            address: '成都市成华区三环路川陕立交桥外侧富森美家居1楼1052',
            tel: '028-61157237',
        }]
    },
    '赤峰': {
        ni: [],
        ne: [{
            name: '红星美凯龙',
            store: 'Chifeng Linhuangdajie Redstar',
            address: '内蒙古赤峰市临潢大街红星美凯龙1F',
            tel: '0476-3995750',
        }]
    },
    '大连': {
        ni: [{
            name: '居然之家',
            store: 'Dalian Easy Home',
            address: '大连市甘井子区西南路71B号居然之家金三角店1号馆1-1-011 NATUZZI专卖店',
            tel: '0411-39689310',
        }, {
          name: '五一大世界',
          store: 'Dalian DSJ',
          address: '大连市沙河口区五一广场五一大世界3楼NATUZZI专卖店',
          tel: '18641108500',
        }],
        ne: [{
            name: '红星美凯龙',
            store: 'Dalian Huananlu Redstar',
            address: '大连市甘井子区华东路与中华西路交汇处红星美凯龙华南商场一楼8006、8007号',
            tel: '0411-39077158',
        }, {
          name: '大连盛祥家居',
          store: 'Dalian kaifuqu Shengxiang',
          address: '大连市金州区辽宁街48-17号',
          tel: '0411-39077158',
        }, {
          name: '大世界家居广场',
          store: '',
          address: '大连市沙河口区五一广场大世界家居广场3楼',
          tel: '18641108500',
        }]
    },
    '大同': {
        ni: [],
        ne: [{
            name: '居然之家',
            store: 'Datong Easy Home',
            address: '大同市永泰南路居然之家三楼3019意迪森沙发',
            tel: '13038090438',
        }]
    },
    '佛山': {
        ni: [],
        ne: [{
            name: '罗浮宫',
            store: 'Foshan Leconglu Luofugong',
            address: '广东省佛山市顺德区325国道乐从路段罗浮宫国际家具博览中心 1FC26/39',
            tel: '0757-28919942',
        }]
    },
    '广州': {
        ni: [{
            name: '番禺吉盛伟邦',
            store: 'Guangzhou Panyu JSWB',
            address: '广州番禺区迎宾路万博中心广州吉盛伟邦家居中心番禺店A馆一层A110',
            tel: '020-84560506',
        }, {
          name: '琶洲吉盛伟邦',
          store: 'Guangzhou Pazhou JSWB',
          address: '广州市海珠区新港东路1000号吉盛伟邦家居5F',
          tel: '020-89305156',
        }],
        ne: [{
            name: '吉盛伟邦番禺店',
            store: 'Guangzhou Panyu JSWB',
            address: '广东省广州市番禺迎宾路吉盛伟邦C馆101-102',
            tel: '020-34821963',
        }, {
            name: '琶洲吉盛伟邦',
            store: 'Guangzhou Pazhou JSWB',
            address: '广东省广州市海珠区新港东路100号吉盛伟邦常年馆2018铺‍',
            tel: '020-89203155',
        }, {
          name: '维家思',
          store: 'Guangzhou Weijiasi Furniture Plaza',
          address: '广东省广州市天河区黄埔大道西188号维家思广场130A号',
          tel: '020-89810796',
        }]
    },
    '贵阳': {
        ni: [{
            name: '红星',
            store: 'Guiyang Redstar',
            address: '贵阳市南明区油榨街10号红星美凯龙二楼B1-8172,8193,8195,8196',
            tel: '0851-85500867',
        }],
        ne: [{
            name: '红星美凯龙',
            store: 'Guiyang Fuyuanmei redstar',
            address: '贵州省贵阳市南明区油榨街10号红星美凯龙',
            tel: '0851-85501909',
        }]
    },
    '哈尔滨': {
        ni: [{
            name: '月星',
            store: 'Harbin Moonstar',
            address: '哈尔滨市道里区上海街66号月星家居A幢一层101',
            tel: '0451-87639120',
        }],
        ne: [{
            name: '先锋居然之家',
            store: 'Xianfeng Easyhome',
            address: '黑龙江省哈尔滨市先锋路471号居然之家三楼3-1-01',
            tel: '0451-55553387',
        }]
    },
    '杭州': {
        ni: [{
            name: '滨江大都会',
            store: 'Hangzhou Derlook2',
            address: '杭州市江南大道1078号大都会家居博览园红9馆 B101和B201',
            tel: '0571-87391806',
        }, {
            name: '杭州西溪',
            store: 'Hangzhou Derlook3',
            address: '杭州文一西路830号杭州第六空间西溪艺术生活广场是一楼F128',
            tel: '0571-87792898',
        }, {
            name: '古墩新时代',
            store: 'Hangzhou Newera Plaza',
            address: '杭州市西湖区古墩路808号新时代A座一楼20-21号',
            tel: '0571-86560189',
        }, {
          name: '城站第六空间',
          store: 'Hangzhou Derlook1',
          address: '杭州市西湖大道18号第六空间艺术生活广场二楼B37',
          tel: '0571-87185539',
        }],
        ne: [{
            name: '六空滨江店',
            store: 'Hangzhou Binjiang Derlook',
            address: '杭州市滨江区江南大道1088号大都会第六空间现代馆B206',
            tel: '0571-86691690',
        }, {
            name: '欧亚达',
            store: 'Hangzhou Qiutaolu OYADA',
            address: '杭州市秋涛北路72号欧亚达1楼A1-228',
            tel: '0571-85390105',
        }, {
          name: '西溪第六空间',
          store: 'Hangzhou Xixi Derlook',
          address: '浙江省杭州市西湖区文一西路830号二楼F208',
          tel: '0571-87039979',
        }]
    },
    '合肥': {
        ni: [],
        ne: [{
            name: '第六空间',
            store: 'Hefei Yuxilu Derlook',
            address: '安徽省合肥市瑶海区裕溪路第六空间一楼A125',
            tel: '0551-68822282',
        }, {
            name: '红星美凯龙',
            store: 'Hefei Zhengwu Redstar',
            address: '安徽省合肥市潜山路与南二环路交叉口向东300米红星美凯龙',
            tel: '0551-62690758',
        }, {
          name: '合肥滨湖居然之家',
          store: '',
          address: '安徽省合肥市滨湖区紫云路世纪金源购物中心D栋居然之家2楼',
          tel: '0551-68168720',
      }]
    },
    '呼和浩特': {
        ni: [{
            name: '居然之家',
            store: 'Hohhot Easyhome',
            address: '呼和浩特市新城区兴安北路169号（蒙苑广场）居然之家A座家之尊B馆口2-1-001',
            tel: '0471-2850680',
        }],
        ne: [{
            name: '红星美凯龙',
            store: 'Hohhot Xinqu Redstar',
            address: '内蒙呼和浩特新城区红星美凯龙1FA8127 A8061 A8062-1',
            tel: '0471-2858995',
        }, {
            name: '居然之家',
            store: 'Hohhot Xinqu Easyhome',
            address: '内蒙呼和浩特新城区居然之家1F3-1-008',
            tel: '0471-2850649',
        }]
    },
    '湖州': {
        ni: [],
        ne: [{
            name: '中和家居城2楼',
            store: 'Huzhou Nanxun Zhonghe Furniture mall',
            address: '湖州市南浔镇中和家居城2楼 Natuzzi Editions',
            tel: '0572-3021788',
        }]
    },
    '济南': {
        ni: [{
            name: '红星美凯龙',
            store: 'Jinan Redstar',
            address: '山东省济南市天桥区北园大街红星美凯龙一楼',
            tel: '13589083266',
        }],
        ne: [{
            name: '济南红星店',
            store: 'Jinan Redstar',
            address: '山东省济南市天桥区北园大街225号1楼西厅',
            tel: '0531-55530686',
        }, {
          name: '济南银座店',
          store: 'Jinan Yinzuo',
          address: '济南北园大街银座家居中心店家具馆一楼西厅意迪森店',
          tel: '0531-55559635',
        }]
    },
    '济宁': {
        ni: [],
        ne: [{
            name: '红星美凯龙',
            store: 'Jining Redstar',
            address: '济宁市琵琶山路与金宇路交汇处红星美凯龙1楼意迪森',
            tel: '13012610500',
        }]
    },
    '嘉兴': {
        ni: [],
        ne: [{
            name: '嘉地广场',
            store: 'Jiaxing Jinxialu Jiadi',
            address: '嘉兴市桐乡大道锦霞路口嘉地广场1楼1039号意迪森',
            tel: '0573-82606338',
        }]
    },
    '昆明': {
        ni: [{
            name: '红星',
            store: 'Kunming Redstar',
            address: '云南省昆明市广福路318号红星美凯龙一楼',
            tel: '18669012090',
        }],
        ne: [{
            name: '红星美凯龙',
            store: 'Kunming Guangfulu Redstar',
            address: '云南省昆明市广福路318号红星美凯龙1楼A8103',
            tel: '0871-64638896',
        }]
    },
    '兰州': {
        ni: [{
            name: '万佳家居',
            store: 'Lanzhou Wanjia',
            address: '兰州天水路兰州居然万佳家居一楼',
            tel: '0931-8517569',
        }],
        ne: [{
            name: '红星美凯龙',
            store: '',
            address: '甘肃省兰州市城关区飞雁路1号红星美凯龙',
            tel: '18693056000',
        }]
    },
    '临沂': {
        ni: [],
        ne: [{
            name: '怡景丽家',
            store: 'Yijinglijia Furniture Mall',
            address: '山东省临沂市兰山区解放路184号怡景丽家1楼B028',
            tel: '0539-8353277',
        }]
    },
    '南京': {
        ni: [{
            name: '第六空间',
            store: 'Nanjing Derlook',
            address: '南京市秦淮区龙蟠中路216号第六空间一楼A121',
            tel: '025-85865566',
        }, {
            name: '金盛国际家具',
            store: 'Jinsheng International Mall',
            address: '南京市建业区江东中路80号金盛国际家具一楼',
            tel: '025-84465566',
        }],
        ne: [{
          name: '浦口红星',
          store: 'Nanjing Pukou Redstar',
          address: '南京市浦口区 浦口大道18号红星美凯龙家居广场1楼4号厅',
          tel: '025-85475566',
        }]
    },
    '南通': {
        ni: [{
            name: '百安宜家',
            store: 'Nantong Baiyijia',
            address: '南通市港闸区濠西路288号百安宜家国际馆2楼C015-2-C018',
            tel: '0513-89026699',
        }],
        ne: [{
          name: '百安谊家',
          store: 'Nantong Haoxilu Baianyijia',
          address: '江苏省南通市港闸区濠西路266号百安谊家三楼 D79+D80-1',
          tel: '0513-55889889',
        }, {
            name: '百安谊家',
            store: 'Nantong Haoxilu Baianyijia',
            address: '江苏省南通市港闸区濠西路266号百安谊家二楼意迪森C003-006',
            tel: '0513-55889889',
        }]
    },
    '南阳': {
      ni: [],
      ne: [{
          name: '红星美凯龙',
          store: '',
          address: '河南省南阳市车站北路红星美凯龙A座',
          tel: '18567265959',
      }]
    },
    '宁波': {
        ni: [{
            name: '第六空间',
            store: 'Ningbo Derlook',
            address: '宁波市江东区宁穿路315号第六空间家居广场1楼A-02',
            tel: '0574-87478192',
        }],
        ne: [{
            name: '第六空间店',
            store: 'Ningbo Ningchuanlu Derlook',
            address: '浙江省宁波市江东区宁穿路315号第六空间国际家居二楼B18意迪森专卖店',
            tel: '0574-87350085',
        }, {
          name: '友邦店',
          store: 'Ningbo Youbang',
          address: '宁波市鄞州区麦德龙路友邦家居1楼1-2A意迪森',
          tel: '0574-82818007',
        }]
    },
    '秦皇岛': {
        ni: [],
        ne: [{
            name: '居然之家',
            store: 'Qinghuangdao Easyhome',
            address: '秦皇岛市海港区秦皇西大街与华山路口居然之家2楼',
            tel: '18833566585',
        }]
    },
    '青岛': {
        ni: [],
        ne: [{
            name: '富尔玛店',
            store: 'Qingdao Liaoyangxilu Former',
            address: '山东省青岛市辽阳西路118号辽阳西路富尔玛国际家居博览中心一层',
            tel: '0532-55575668',
        }]
    },
    '泉州': {
        ni: [],
        ne: [{
            name: '现代家居',
            store: 'Quanzhou Quanxiulu Xiandai',
            address: '福建省泉州市泉秀路现代家居广场精品馆109',
            tel: '0595-28221881',
        }]
    },
    '厦门': {
        ni: [{
          name: '红星',
          store: '',
          address: '厦门市湖里区钟宅路69号红星美凯龙三楼C8036Natuzzi',
          tel: '0592-5790661',
        }],
        ne: [{
            name: '莲坂侨星店',
            store: 'Xiamen Jiahe Banqiaoxing',
            address: '厦门市嘉禾路92号莲坂侨星大厦一、二楼',
            tel: '0592-5081736',
        }, {
            name: '红星美凯龙店',
            store: 'Xiamen Zhongzhailu Redstar',
            address: '福建省厦门市湖里区钟宅路69号3楼C8049-C8051意迪森专卖店',
            tel: '0592-5702293',
        }]
    },
    '上海': {
        ni: [{
            name: '澳门路月星',
            store: 'Shanghai Moonstar',
            address: '上海澳门路168号上海月星家居广场一楼A060 - 063',
            tel: '021-62481357',
        }, {
            name: '文定家居',
            store: 'Shanghai Wending',
            address: '上海文定路258号文定生活家居创意广场A107-207',
            tel: '021- 64416867',
        }, {
            name: '虹桥吉盛伟邦',
            store: 'Shanghai Hongqiao JSWB',
            address: '上海娄山关路75号上海吉盛伟邦虹桥店4楼405-406',
            tel: '021-62369392',
        }, {
            name: '吴中路红星',
            store: 'Shanghai Wuzhong Redstar',
            address: '上海吴中路1388号红星美凯龙二楼B8026-8031',
            tel: '021-64059508',
        }, {
            name: '青浦吉盛伟邦',
            store: 'Shanghai Qingpu JSWB',
            address: '上海青浦区赵巷镇嘉松中路5369号吉盛伟邦家具村B6/101室',
            tel: '021-59755930',
        }],
        ne: [{
            name: '浦东红星',
            store: 'Shanghai Hunanlu Redstar',
            address: '上海市浦东新区沪南路2178号红星美凯龙G8026-8027',
            tel: '021-50253899',
        }, {
            name: '澳门路月星',
            store: 'Shanghai Aomenlu Moonstar',
            address: '上海市澳门路168号月星家居二楼B027-028',
            tel: '021-62768880',
        }, {
            name: '青浦吉盛伟邦',
            store: 'Shanghai Qingpu JSWB',
            address: '上海市青浦区赵巷镇嘉松中路5369号吉盛伟邦A6-205',
            tel: '021-69755013',
        }, {
            name: '吴中路红星',
            store: 'Shanghai Wuzhonglu Redstar',
            address: '上海市吴中路1388号1幢三楼C8032',
            tel: '021-64196996',
        }, {
            name: '松江月星',
            store: 'Shanghai Songjiang Moonstar',
            address: '上海市松江区广富林路799弄月星家居一楼A087-090',
            tel: '021-37656061',
        }, {
            name: '文定家居',
            store: 'Shanghai Wending',
            address: '上海市徐汇区文定路258号家居创意广场',
            tel: '021-64261651',
        }, {
            name: '汶水路红星',
            store: 'Shanghai Wenshuilu Redstar',
            address: '上海市汶水路1555号红星美凯龙一楼A8279',
            tel: '021-61175058',
        }]
    },
    '绍兴': {
        ni: [{
          name: '第六空间',
          store: 'Shaoxing Erhuanlu Derlook',
          address: '浙江省绍兴市二环北路38号第六空间一层1-015',
          tel: '0575-88800888',
        }],
        ne: [{
            name: '第六空间',
            store: 'Shaoxing Erhuanlu Derlook',
            address: '浙江省绍兴市二环北路38号第六空间家具城1F-017',
            tel: '0575-85020001',
        }]
    },
    '深圳': {
        ni: [{
            name: '星河第三空间',
            store: 'Shenzhen Topliving',
            address: '深圳市福田区彩田路3069号星河世纪大厦星河第三空间1层1014单元和B1层5005、5006单元',
            tel: '0755-26941158',
        }, {
            name: '福田区红星',
            store: 'Shenzhen Redstar',
            address: '深圳市福田区深圳大道北侧香蜜湖世纪中心红星美凯龙一层1B021-022店铺',
            tel: '0755-83213913',
        }],
        ne: [{
            name: '好百年总店',
            store: 'Shenzhen Baoanlu Haobainian',
            address: '广东省深圳市罗湖区宝安北路3019号好百年总店2F意迪森',
            tel: '0755-22305951',
        }, {
            name: '香蜜湖红星',
            store: 'Shenzhen Redstar Xiangmihu',
            address: '深圳市香蜜湖路红星美凯龙世纪中心一楼意迪森',
            tel: '0755-82569303',
        }, {
            name: '香江家居',
            store: 'Shenzhen Xiangjiang Furniture',
            address: '深圳市南山区沙河东路欧洲城香江家居1楼意迪森店',
            tel: '0755-26914473',
        }, {
          name: '富邦红树湾',
          store: 'Shenzhen Homson Furniture',
          address: 'Shenzhen Homson Furniture',
          tel: '023-86825199',
        }]
    },
    '沈阳': {
        ni: [{
          name: '居然之家',
          store: 'Shenyang Easyhome',
          address: '沈阳市浑南新区金卡路16号 沈阳奥体中心路南即是',
          tel: '024-85637679',
        }],
        ne: [{
          name: '居然之家浑南店',
          store: 'Shenyang Easyhome',
          address: '辽宁省沈阳市浑南区金卡路在地图中查看16号',
          tel: '024-85637679',
        }]
    },
    '石家庄': {
      ni: [{
        name: '红星美凯龙',
        store: 'Shijiazhuang Redstar',
        address: '河北省石家庄市裕华东路133号红星美凯龙方北商场一楼C8138号展位',
        tel: '13231122111',
      }],
      ne: [{
        name: '怀特家居',
        store: 'Shijiazhuang Huaite',
        address: '河北省石家庄市裕华区育才街265号怀特国际家居广场家具馆一楼',
        tel: '15003314661',
      }]
    },
    '苏州': {
        ni: [{
            name: '嘉实店',
            store: 'Suzhou (free standing)',
            address: '苏州市工业园区苏州大道西313号嘉实大厦1楼',
            tel: '0512-67261500',
        }, {
          name: '新光天地',
          store: 'Shinkong',
          address: '苏州工业园区苏州大道东456号商场1层01012号',
          tel: '0512-67162852',
        }],
        ne: [{
            name: '金鸡湖第六空间',
            store: '',
            address: '苏州市工业园区望墩路金鸡湖商业广场第六空间现代馆',
            tel: '',
        }]
    },
    '台州': {
        ni: [],
        ne: [{
            name: '第六空间',
            store: 'Taizhou Jiaojiangqu Derlook',
            address: '浙江省台州市椒江区洪家装饰城隔壁第六空间国际家居2楼B39',
            tel: '0576-89230039',
        }]
    },
    '太原': {
        ni: [{
            name: '居然之家',
            store: 'Taiyuan Easyhome',
            address: '太原市长治路185号 居然之家家具家饰馆一层1楼',
            tel: '0351-7535918',
        }],
        ne: [{
            name: '居然之家春天店',
            store: 'Taiyuan Chuntian Easyhome',
            address: '山西省太原市长治路与长风大街 居然之家 1F',
            tel: '0351-7535697',
        }, {
          name: '居然之家河西店',
          store: 'Taiyuan Hexi Easyhome',
          address: '山西省太原市迎泽西大街332号居然之家1楼1006',
          tel: '0351-7535697',
        }]
    },
    '唐山': {
        ni: [],
        ne: [{
            name: '常记家居',
            store: 'Tangshan Longzelu Changji',
            address: '河北省唐山市高新区龙泽北路与龙华东道交叉口常记家居',
            tel: '0315-6710883',
        }]
    },
    '天津': {
        ni: [{
            name: '加宜河西家居店',
            store: 'Tianjin Hexi Jiayi',
            address: '天津市河西区友谊路16号加宜家居博览中心一层展区A001号',
            tel: '022-88370368',
        }],
        ne: [{
            name: '加宜河西家居店',
            store: 'Tianjin Hexi Jiayi',
            address: '天津市河西区友谊路16号加宜家居博览中心一层展区A260号',
            tel: '022-88370358',
        }, {
            name: '加宜和平家居店',
            store: 'Tianjin Heping Jiayi',
            address: '天津市和平区南门外大街7号加宜家居博览中心一层展区A001号',
            tel: '022-27358589',
        }]
    },
    '威海': {
        ni: [],
        ne: [{
            name: '居然之家高新区店',
            store: '',
            address: '威海市经济技术开发区华夏路18号居然之家威海店二楼',
            tel: '0631-5210989',
        }]
    },
    '温州': {
        ni: [],
        ne: [{
            name: '联结福家居',
            store: 'Wenzhou Wenzhoudadao Lianjiefu',
            address: '浙江省温州市龙湾区温州大道690号联结福国际家居广场3楼B16',
            tel: '0577-86381818',
        }]
    },
    '无锡': {
        ni: [{
            name: '第六空间',
            store: 'Wuxi Derlook',
            address: '无锡市锡沪东路201号第六空间家居生活广场一层A108',
            tel: '0510-82038116',
        }],
        ne: []
    },
    '芜湖': {
        ni: [],
        ne: [{
            name: '居然之家',
            store: 'Wuhu Easyhome',
            address: '安徽省芜湖市鸠江区居然之家一楼意迪森店',
            tel: '0553-3908882 / 3908883',
        }, {
            name: '奥体店',
            store: '',
            address: '安徽省芜湖市弋江区奥体红星美凯龙一楼意迪森独立店',
            tel: '0553-2937166',
        }]
    },
    '武汉': {
        ni: [{
            name: '居然之家武昌',
            store: 'Wuhan Easyhome Wuchang',
            address: '武汉市武昌区徐东大街50号一楼纳图兹店',
            tel: '',
        }],
        ne: [{
            name: '红星美凯龙竹叶山',
            store: 'Redstar Zhuyeshan',
            address: '武汉市江岸区竹叶山红星美凯龙一楼A8053,意迪森',
            tel: '027-51873903',
        }, {
            name: '汉西红星美凯龙店',
            store: '',
            address: '武汉市硚口区汉西三路红星美凯龙四楼意迪森',
            tel: '15607172960',
        }]
    },
    '乌鲁木齐': {
        ni: [],
        ne: [{
            name: '万佳国际',
            store: '',
            address: '乌鲁木齐苏州东路与长春路交叉口万佳国际家居2楼2A21',
            tel: '0991-6680029',
        }]
    },
    '西安': {
        ni: [{
            name: '居然之家高新店',
            store: "Xi'an gaoxin EH",
            address: '西安区雁塔区高新四路居然之家',
            tel: '029-65653307',
        }],
        ne: [{
            name: '太白路红星美凯龙',
            store: 'Taibai Road Redstar',
            address: '陕西省西安市太白路1号红星美凯龙3楼3-1-015',
            tel: '029-62991811',
        }, {
            name: '南大明宫',
            store: 'Xi’an NDMG',
            address: '西安市雁塔区含光南路与电子二路交汇处南大明宫三楼西厅意迪森',
            tel: '029-87301883',
        }, {
          name: '居然之家中联店',
          store: 'Xi‘an zhonglian Easyhome',
          address: '陕西西安市未央区北二环路西段明光路口',
          tel: '029-89820360',
        }]
    },
    '廊坊': {
        ni: [],
        ne: [{
            name: '香河金钥匙店',
            store: 'Xianghe Xiushui Street Jinyaoshi Store',
            address: '河北省廊坊市香河县秀水街31号金钥匙家具城2F',
            tel: '0316-5189163',
        }]
    },
    '襄阳': {
        ni: [{
            name: '天丽家居',
            store: 'Xiangyang Tianli',
            address: '湖北省襄阳市前进路9号天丽家居一楼',
            tel: '0710-3318799',
        }],
        ne: [{
            name: '天丽家居店',
            store: 'Xiangyang Qianjinlu Tianli',
            address: '湖北省襄阳市樊城区前进路9号天丽家居一楼A区12',
            tel: '0710-3461609',
        }]
    },
    '徐州': {
        ni: [],
        ne: [{
            name: '复兴北路红星一期',
            store: 'Xuzhou Fuxingbeilu Redstar',
            address: '徐州市鼓楼区复兴北路红星美凯龙一期一楼',
            tel: '18606161366',
        }]
    },
    '宜昌': {
        ni: [],
        ne: [{
            name: '欧亚达',
            store: 'Yichang Dongshan Oyada',
            address: '湖北省宜昌市东山开发区金东山欧亚达家居二楼A2-16',
            tel: '0717-6215000',
        }]
    },
    '义乌': {
        ni: [],
        ne: [{
            name: '义乌家具市场',
            store: 'Yiwu Furniture Mall',
            address: '浙江省义乌市西城路1779号义乌家具市场B14意迪森',
            tel: '0579-85530599',
        }]
    },
    '银川': {
      ni: [],
      ne: [{
          name: '红星美凯龙',
          store: 'Yinchuan Redstar',
          address: '宁夏回族自治区银川市兴庆区丽景南街红星美凯龙家居生活广场五楼',
          tel: '15349617770',
      }]
    },
    '张家港': {
        ni: [],
        ne: [{
            name: '九州国际家居',
            store: 'Zhangjiagang Jingangdadao Jiuzhou',
            address: '江苏省张家港市金港大道与南二环交接处九州国际家居城1F-1003',
            tel: '0512-58607899',
        }, {
          name: '月星家居',
          store: 'Zhangjiagang Moonstar',
          address: '江苏省张家港市金港大道与南二环交接处月星家居城1F-1003',
          tel: '0512-58607899',
        }]
    },
    '长春': {
        ni: [],
        ne: [{
            name: '居然之家',
            store: 'Changchun Saide EH',
            address: '长春市二道区赛德广场居然之家二楼意迪森沙发',
            tel: '13756009756',
        }]
    },
    '郑州': {
        ni: [{
            name: '欧凯龙',
            store: 'Zhengzhou Ocalone',
            address: '郑州市农业路与商都路交叉口欧凯龙家居馆1楼北门1号',
            tel: '0371-55690099',
        }],
        ne: [{
            name: '东区欧凯龙2楼',
            store: 'Zhengzhou Ocalone',
            address: 'Furniture mall 郑州市农业路与商都路交叉口欧凯龙家居馆2楼 Natuzzi Editions',
            tel: '0371-55690099',
        }]
    },
    '重庆': {
        ni: [{
            name: '居然金源店',
            store: 'Chongqing easyhome',
            address: '重庆江北区金源路居然之家1楼NATUZZI',
            tel: '18680755689',
        }],
        ne: [{
            name: '聚信店',
            store: 'Chongqing Jinkaidadao Juxinmei',
            address: '重庆市北部新区金开大道1222号聚信美家居世纪城1A-02号',
            tel: '023-63083883',
        }, {
            name: '二郎红星',
            store: 'Chongqing Erlang Redstar',
            address: '重庆市九龙坡区二郎迎宾大道11号红星美凯龙4楼E8040-8041',
            tel: '023-68967036',
        }, {
            name: '居然之家金源',
            store: 'Chongqing Jinyuan Easyhome',
            address: '重庆市江北区北滨路369号居然之家金源店2楼E-2016',
            tel: '023-86825199',
        }]
    },
    '珠海': {
        ni: [{
            name: '世邦家居',
            store: 'Zhuhai Cibo Home',
            address: '珠海香洲前山三台石路31号世邦家居世界1楼',
            tel: '13392225521',
        }],
        ne: []
    }
};